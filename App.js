/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import PlaceInput from './src/components/PlaceInput/PlaceInput';
import PlaceList from './src/components/PlaceList/PlaceList';
import placeImage from './src/assets/images/lakecomo.jpg';
import PlaceDetail from './src/components/PlaceDetail/PlaceDetail';
import { connect } from 'react-redux';
import { addPlace, deletePlace, deselectPlace, selectPlace} from './src/store/actions/index';
type Props = {};
class App extends Component<Props> {

  placeAddedHandler = (name)=>{
    this.props.onAddPlace(name);
  };

  placeDeletedHandler = ()=>{
    this.props.onDeletePlace();
  };
  closeModalHandler = ()=>{
    this.props.onDeselectPlace();
  };
  placeSelectedHandler = (index)=>{
    this.props.onSelectPlace(index);
  };

  render() {

    return (
      <View style={styles.container}>
        <PlaceDetail selectedPlace={this.props.placeSelected}
                     onItemDeleted={this.placeDeletedHandler}
                     onModalClosed={this.closeModalHandler}
        />
        <PlaceInput
            onPlaceAdded={this.placeAddedHandler}/>
        <PlaceList
            onItemSelected={this.placeSelectedHandler}
            places={this.props.places}/>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "90%",
    marginLeft: "5%",
    flex: 1,
    paddingTop: 106,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#fff",
  },



});

const mapStateToProps = state => {
  return {
    places: state.places.places,
    placeSelected: state.places.placeSelected
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onAddPlace: (placeName) => dispatch(addPlace(placeName)),
    onDeletePlace: () => dispatch(deletePlace()),
    onSelectPlace: (key) => dispatch(selectPlace(key)),
    onDeselectPlace: () => dispatch(deselectPlace())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);