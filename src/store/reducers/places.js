import {ADD_PLACE, DELETE_PLACE, DESELECT_PLACE, SELECT_PLACE} from "../actions/actionTypes";
import placeImage from "../../assets/images/lakecomo.jpg";

const initialState = {
    places: [],
    placeSelected: null
};
const reducer = (state = initialState, action) =>{

    switch (action.type) {

        case ADD_PLACE:
            return {
                ...state,
                places: state.places.concat({
                    key: Math.random().toString(),
                    name:action.placeName,
                    image: {
                        uri: 'http://images2.fanpop.com/image/photos/8800000/ook-terry-pratchett-8884306-1680-1050.jpg'
                    }
                })
            };
        case DELETE_PLACE:
            return {
                ...state,
                places: state.places.filter((place) => {
                    return (state.placeSelected.key!==place.key);
                }),
                placeSelected: null
            };

        case SELECT_PLACE:
            return {
                ...state,
                placeSelected: state.places.find((place) => {
                    return (place.key===action.placeKey);
                })
            };

        case DESELECT_PLACE:
            return {
                ...state,
                placeSelected: null
            };

        default:
            return state;

    }
};

export default reducer;