import React from 'react';
import {Modal,View, Image, Text, Button, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const placeDetail = props => {
    let modalContent = null;
    if (props.selectedPlace) {
        modalContent = (
            <View>
                <Image style={styles.image} source={props.selectedPlace.image}/>
                <Text style={styles.placeName}>{props.selectedPlace.name}</Text>
            </View>
        );
    }
    return (
    <Modal visible={props.selectedPlace !== null} animationType="slide">
        <View style={styles.modalContainer}>
            { modalContent }
            <View style={styles.buttonsContainer}>
                <TouchableOpacity
                    onPress={props.onItemDeleted}
                >
                    <View style={styles.deleteButton} >
                    <Icon size={30} name="ios-trash" color="red"/>
                    </View>
                </TouchableOpacity>
                <Button title="Close"
                        onPress={props.onModalClosed}
                />
            </View>
        </View>
    </Modal>
)
};

const styles = StyleSheet.create({
    modalContainer: {
        margin: 22
    },
    buttonsContainer: {
    },
    image : {
        marginTop:30,
        width: '100%',
        height: 300,
        resizeMode: 'contain'
    },
    placeName: {
        fontSize: 28,
        textAlign: 'center'
    },
    deleteButton: {
        alignItems: 'center'

    }
});

export default placeDetail;