import React , { Component } from 'react';
import {Button, TextInput, View, StyleSheet} from "react-native";

class PlaceInput extends Component {

    state = {
        placeName:'',
    };

    placeSubmitHandler = () =>{
        if (this.state.placeName.trim() === "") {
            return;
        }

        this.props.onPlaceAdded(this.state.placeName);
        this.setState({
            placeName:'',
        });


    };

    placeNameChangedHandler = (val)=>{
        this.setState({
            placeName: val
        });
    };
    render(){

        return (
        <View style={styles.inputContainer}>
            <TextInput style={styles.placeInput}
                       onChangeText={this.placeNameChangedHandler}
                       value={this.state.placeName}>
            </TextInput>
            <Button
                onPress={this.placeSubmitHandler}
                title="Add"
                style={styles.placeButton} />
        </View>
        );
    }
}

const styles = StyleSheet.create({

    inputContainer:{
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        height: 20,
        alignItems: "center",

    },
    placeInput :{
        width: "70%",
         borderColor: "black",
        borderWidth:1
    },
    placeButton: {
        backgroundColor:'yellow',
        color: 'red',
        width:"30%",
        alignItems: "center",
    }
});

export default PlaceInput;