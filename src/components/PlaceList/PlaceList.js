import React from 'react';
import {} from 'react-native';
import {View, StyleSheet, FlatList} from "react-native";
import ListItem from "../ListItem/ListItem";
const placeList = props => {

    return  (
    <FlatList style={styles.listContainer}
              renderItem={(info)=>{
                  return (
                  <ListItem
                      placeName={info.item.name}
                      placeImage={info.item.image}
                      onItemPressed={()=>{props.onItemSelected(info.item.key)}}
                  />)
              }}
              data={props.places}
    />)
    };


const styles = StyleSheet.create({
    listContainer: {
          width: '100%',
         // backgroundColor: '#AAA'
    }
});

export default placeList;